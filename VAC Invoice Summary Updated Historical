DROP table if exists sand_comops.cast_vac_invoice_summary_hist;

CREATE table sand_comops.cast_vac_invoice_summary_hist partitioned by (gl_date_par)
stored as parquet LOCATION 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_comops.db/cast_vac_invoice_summary_hist' as
--explain
select 
    pa.party_name,
    ca.ACCOUNT_NUMBER,
    cb.account_number as bill_to_number,
    pb.party_name as bill_to_name,    
    sum(k.REVENUE_AMOUNT) as REVENUE_AMOUNT,
    sum(k.THERAPY_DAYS) as THERAPY_DAYS,
    pd.PRODUCT_ITEM_DESCRIPTION,
    k.RENTAL_SALE_FLAG,
    k.TRX_TYPE,
    sum(k.QUANTITY_SOLD) as QUANTITY_SOLD,
    k.START_DATE,
    k.END_DATE,
    k.ORDER_NUMBER,
    k.GL_DATE,
    k.ITEM_NUMBER,
    k.ORIG_ORDER_NUM,
    k.TRX_NUMBER,
    k.TRX_DATE,
    k.PATIENT_FIRST_NAME,
    k.PATIENT_LAST_NAME,
    -- ol.ATTRIBUTE8,
    k.PO_NUMBER,
    -- gl.SEGMENT1,
    -- ffv.DESCRIPTION as DESCRIPTION1,
    k.REASON_CODE,
    -- csu.SITE_USE_ID,
    ktr.TERRITORY_CODE,
    pd.PRODUCT_BRAND,
    k.gl_date_par

from ebs_usawt.apps_kar_revenue k

/*
join ebs_usawt.apps_gl_code_combinations gl
on k.CODE_COMBINATION_ID = gl.CODE_COMBINATION_ID  
and gl.SEGMENT1 in ('2010','2020')
and k.gl_date_par >= 201701
and DECODE( k.REV_TYPE,'FRT','BILLED FREIGHT',decode( k.THERAPY_CATEGORY,NULL,'No Therapy Category Assigned',
  'ROTOPRONE','CRITICAL CARE', k.THERAPY_CATEGORY))  =  'VAC'
and k.set_of_books_id = 1 
*/

join aim.aim_product_dim_new pd
on pd.product_sku = k.item_number
and pd.inventory_item_id = k.inventory_item_id
and k.gl_date_par >= 201401 and k.gl_date_par < 201901
and DECODE( k.REV_TYPE,'FRT','BILLED FREIGHT',decode( k.THERAPY_CATEGORY,NULL,'No Therapy Category Assigned',
  'ROTOPRONE','CRITICAL CARE', k.THERAPY_CATEGORY))  =  'VAC'
and k.set_of_books_id = 1 

/*
join ebs_usawt.apps_hz_cust_site_uses_all csu
on csu.site_use_id = k.ship_to_site_use_id
and csu.site_use_code = 'SHIP_TO'

join ebs_usawt.apps_hz_cust_acct_sites_all cas
on cas.cust_acct_site_id = csu.cust_acct_site_id
*/

join ebs_usawt.apps_hz_cust_accounts ca
on ca.cust_account_id = k.ship_to_customer_id
--on ca.cust_account_id = cas.cust_account_id

join ebs_usawt.apps_hz_cust_accounts cb
on cb.cust_account_id = k.bill_to_customer_id

join ebs_usawt.apps_hz_parties pa 
on pa.party_id = ca.party_id
and pa.party_type <> "PARTY_RELATIONSHIP"

join ebs_usawt.apps_hz_parties pb 
on pb.party_id = cb.party_id
and pb.party_type <> "PARTY_RELATIONSHIP"

/*
join EBS_USAWT.APPS_FND_FLEX_VALUES_VL ffv
on ffv.flex_value = gl.segment1
and ffv.flex_value_set_id = 1022358 
*/

join ebs_usawt.apps_kar_site_salesreps ks
on k.SHIP_TO_SITE_USE_ID= ks.SITE_USE_ID AND ks.primary_flag = 'Y' 
AND ks.end_date_active is null

/*join ebs_usawt.apps_oe_order_lines_all ol
on ol.header_id = k.header_id
and ol.line_id = k.line_id
and ol.creation_date >= '2017-01-01 00:00:00'
*/

join ebs_usawt.APPS_KAR_TERRITORY_RD_VW ktr
on ktr.zs_territory_id = ks.zs_territory_id
and ( ktr.territory_code like "TSV%" or ktr.territory_code like "TMV%" )

group by 
    pa.party_name,
    ca.ACCOUNT_NUMBER,
    cb.account_number,
    pb.party_name,    
    pd.PRODUCT_ITEM_DESCRIPTION,
    k.RENTAL_SALE_FLAG,
    k.TRX_TYPE,
    k.START_DATE,
    k.END_DATE,
    k.ORDER_NUMBER,
    k.GL_DATE,
    k.ITEM_NUMBER,
    k.ORIG_ORDER_NUM,
    k.TRX_NUMBER,
    k.TRX_DATE,
    k.PATIENT_FIRST_NAME,
    k.PATIENT_LAST_NAME,
  --   ol.ATTRIBUTE8,
    k.PO_NUMBER,
    -- gl.SEGMENT1,
    -- ffv.DESCRIPTION,
    k.REASON_CODE,
    -- csu.SITE_USE_ID,
    ktr.TERRITORY_CODE,
    pd.PRODUCT_BRAND,
    k.gl_date_par
    
COMPUTE STATS sand_comops.cast_vac_invoice_summary_hist;