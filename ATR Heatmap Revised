drop table if exists sand_cast.heatmap_atr_2020;

Create table sand_cast.heatmap_atr_2020 stored as parquet
LOCATION 'hdfs://S21090101-SA-hdfs01.kci.com:8020/user/hive/warehouse/sand_cast.db/heatmap_atr_2020' as
SELECT atr2.*,
month(atr2.monthday_date) cal_month,
floor(month(atr2.monthday_date)/3) cal_qtr,
year(atr2.monthday_date) cal_year,
atr4.account_performance,
corc.capital_units,
corc.convenience_units,
corc.cap_or_cnv_units
FROM
(SELECT
atr.monthday_date,
atr.geography,
atr.geography_leader,
atr.Region, 
atr.rvp_name,
atr.District,
atr.dm_name,
atr.customer_account_number, 
atr.Account_name, 
atr.customer_market_category, 
atr.customer_market_segment, 
atr.ATR, 
atr.ATR_terr,
atr.TMV, 
atr.TMV_terr,
sum(atr.transitions) transitions,
sum(atr.commissionable_orders) commissionable_orders,
sum(atr.express_orders) express_orders
FROM
(SELECT
f.monthday_date,
s.geography,
s.geography_leader,
s.Region, 
s.rvp_name,
s.District,
s.dm_name,
ac.customer_account_number, 
ac.customer_name Account_name, 
ac.customer_market_category, 
ac.customer_market_segment, 
s.salesrep_name ATR, 
s.territory_code ATR_terr,
sd.salesrep_name TMV,
sd.territory_code TMV_terr,
sum(f.transitions) transitions,
0 commissionable_orders,
0 express_orders
FROM aim.aim_orders_fact f
JOIN aim.aim_orders_dim od on f.order_key = od.order_key 
AND od.billable_flag IN ('Y', 'S') 
AND f.monthday_date >= date_sub(now(), 731)
AND f.monthday_date < now()
AND f.transitions != 0
AND od.rop_type = 'VAC' 
JOIN aim.aim_customer_dim ac on f.transition_facility_key = ac.customer_key 
AND ac.customer_market_category IN ('ACUTE')
AND ac.customer_indirect_flag = 'N' 
JOIN aim.aim_customer_dim ad on f.customer_shipto_key = ad.customer_key
AND ad.customer_market_category <> "ACUTE"
LEFT JOIN aim.aim_customer_zipterr_xref_transposed_vw s ON ac.customer_key = s.customer_key
AND s.sales_credit_type_code = "ATR"
LEFT JOIN aim.aim_customer_zipterr_xref_transposed_vw sd ON ac.customer_key = sd.customer_key
AND (sd.sales_credit_type_code LIKE 'TMV%' OR sd.sales_credit_type_code LIKE 'TDV%')
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15

UNION ALL

SELECT 
f.monthday_date,
s.geography,
s.geography_leader,
s.Region, 
s.rvp_name,
s.District,
s.dm_name,
ac.customer_account_number, 
ac.customer_name Account_name, 
ac.customer_market_category, 
ac.customer_market_segment, 
s.salesrep_name ATR, 
s.territory_code,
sd.salesrep_name TMV,
sd.territory_code, 
0 transitions,
(sum(f.ro_created) - sum(f.ro_cancelled)) commissionable_orders,
sum(case when od.rop_order_source = "KCI Express" then f.ro_created - f.rop_cancelled else 0 end) as express_orders
FROM aim.aim_orders_fact f
JOIN aim.aim_orders_dim od on f.order_key = od.order_key 
AND od.billable_flag IN ('Y', 'S') 
AND f.monthday_date >= date_sub(now(), 731)
AND f.monthday_date < now()
AND od.rop_type = 'VAC' 
JOIN aim.aim_customer_dim ac on f.customer_shipto_key = ac.customer_key 
AND ac.customer_market_category IN ('ACUTE')
AND ac.customer_indirect_flag = 'N' 
LEFT JOIN aim.aim_customer_zipterr_xref_transposed_vw s ON ac.customer_key = s.customer_key
AND s.sales_credit_type_code = "ATR"
LEFT JOIN aim.aim_customer_zipterr_xref_transposed_vw sd ON ac.customer_key = sd.customer_key
AND (sd.sales_credit_type_code LIKE 'TMV%' OR sd.sales_credit_type_code LIKE 'TDV%')
GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15) atr

GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15) atr2

left join 
(
select 
coalesce(cap.account_number, cast(cnv.ship_to_customer_number as string)) as account_number,
COALESCE(cap.capital_units,0) as capital_units, 
COALESCE( cast(cnv.estimated_committed_units as double),0) as convenience_units,
(COALESCE(cap.capital_units,0) + dround( COALESCE( cast(cnv.estimated_committed_units as double),0) )) as cap_or_cnv_units
from
(
select account_number, count(serial_number) as capital_units
from aim_reporting.aim_capital_units_dim 
where effective_start_date >= date_sub(now(), 366) 
group by 1 ) cap

full join aim_reporting.aim_convenience_units_dim cnv
on cap.account_number = cast(cnv.ship_to_customer_number as string)

) corc on atr2.customer_account_number = corc.account_number

left join
(
select
atr3.customer_account_number,
atr3.customer_name,
case when atr3.cy_twelve_transitions > atr3.py_twelve_transitions and atr3.cy_three_transitions <= atr3.py_three_transitions then "Decelerating"
when atr3.cy_twelve_transitions > atr3.py_twelve_transitions and atr3.cy_three_transitions > atr3.py_three_transitions then "Accelerating"
when atr3.cy_twelve_transitions <= atr3.py_twelve_transitions and atr3.cy_three_transitions <= atr3.py_three_transitions then "Underperforming"
when atr3.cy_twelve_transitions <= atr3.py_twelve_transitions and atr3.cy_three_transitions > atr3.py_three_transitions then "Recovering" END as account_performance
FROM
(
select
ac.customer_account_number,
ac.customer_name,
sum(case when f.monthday_date >= date_sub(now(), 366) then f.transitions else 0 end) as cy_twelve_transitions,
sum(case when f.monthday_date >= date_sub(now(), 91) then f.transitions else 0 end) as cy_three_transitions,
sum(case when f.monthday_date >= date_sub(now(), 731) and f.monthday_date < date_sub(now(), 366) then f.transitions else 0 end) as py_twelve_transitions,
sum(case when f.monthday_date >= date_sub(now(), 456) and f.monthday_date < date_sub(now(), 366) then f.transitions else 0 end) as py_three_transitions
FROM aim.aim_orders_fact f
JOIN aim.aim_orders_dim od on f.order_key = od.order_key 
AND od.billable_flag IN ('Y', 'S') 
AND f.monthday_date >= date_sub(now(), 731)
AND f.monthday_date < now()
AND od.rop_type = 'VAC' 
JOIN aim.aim_customer_dim ac on f.transition_facility_key = ac.customer_key 
AND ac.customer_market_category IN ('ACUTE')
AND ac.customer_indirect_flag = 'N' 
JOIN aim.aim_customer_dim ad on f.customer_shipto_key = ad.customer_key
AND ad.customer_market_category <> "ACUTE"
GROUP BY 1, 2) atr3
) atr4 on atr2.customer_account_number = atr4.customer_account_number
and atr2.Account_name = atr4.customer_name;

COMPUTE STATS sand_cast.heatmap_atr_2020;